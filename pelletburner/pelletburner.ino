#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP_Mail_Client.h>

#include "anslutning.h"
int greenLed = 5;   // Kopplad till D1
int redLed = 4;     // Kopplad till D2


SMTPSession smtp;

// Declare the global used Session_Config for user defined session credentials
Session_Config config;


void setup(){
  
  Serial.begin(115200);
  Serial.println();
  pinMode(greenLed, OUTPUT);
  pinMode(redLed, OUTPUT);
}


void testmsg(){
  Serial.println("Nu är LDR belyst");
  //delay(500);
}


void sendmail(){
ESP_Mail_Session session;

  digitalWrite(redLed, HIGH);                     // Tänd röd led för att visa att ett meddelande ska skickas och att programmet nu befinner sig i triggat tillstånd

// *******************************************   Testar att lägga in denna del från setup
Serial.print("Connecting...");
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  while (WiFi.status() != WL_CONNECTED){
    Serial.print(".");
    delay(800);                                    // Ökade pausen från 200ms till 800ms
  }
  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.println();
  smtp.debug(1);
// *******************************************   Här slutar testdelen

  configTime(1, 0, "pool.ntp.org", "time.nist.gov");
 // config.time.ntp_server = "pool.ntp.org,time.nist.gov";
 // config.time.gmt_offset = 1;
 // config.time.day_light_offset = 0;

  session.server.host_name = SMTP_server ;
  session.server.port = SMTP_Port;
  session.login.email = sender_email;
  session.login.password = sender_password;
  session.login.user_domain = "mydomain.com";

  /* Declare the message class */
  SMTP_Message message;
  message.sender.name = "Felindikering";
  message.sender.email = sender_email;
  message.subject = "Pelletspannan";

  /* Försök med multipla mottagare */
  message.addRecipient("Meddelande",Recipient_email_1);     // sm7ytm@gmail.com
  message.addRecipient("Meddelande",Recipient_email_2);     // susanne43jonsson@gmail.com 
  message.addRecipient("Meddelande",Recipient_email_3);     // johanna.jonsson1916@gmail.com

  //Send simple text message
  String textMsg = "Misslyckad start";
  message.text.content = textMsg.c_str();
  message.text.charSet = "utf-8";                                              // Byter us-ascii mot utf-8
  message.text.transfer_encoding = Content_Transfer_Encoding::enc_qp;          // Byter enc_7bit mot enc_qp

  if (!smtp.connect(&session))
    return;

  if (!MailClient.sendMail(&smtp, &message))
    Serial.println("Error sending Email, " + smtp.errorReason());

  delay(3600000);                                                                 // Här ska en paus på 3600000ms (1h) läggas in för att endast ett mail per timme ska skickas
  digitalWrite(redLed, LOW);                                                    // Släck röd led när tiden för triggertillståndet gått ut och programmet återvänder till read_ldr (sedermera till loopen)
}


void read_ldr(){
  digitalWrite(greenLed, LOW);          // Släck grön LED för att visa att programmet lämnat loopen
  int sensorValue = analogRead(A0);     // Läs värdet på analog pin A0
  Serial.println(sensorValue);          // Skriv läst värde på seriell terminal  
  if (sensorValue >= 40){              // Avgör om värdet är över 300 för att på så sätt bestämma att LDR är belyst
    testmsg();                          // om det är så kalla på testmsg
    sendmail();                         // skicka ett mail också
  }                                      
}
 
  
void loop(){
 read_ldr();                           // Kalla på rutinen för att läsa om LDR är belyst (larm-LED på brännaren har löst ut)
 digitalWrite(greenLed, HIGH);         // Tänd grön LED för att visa att programmet befinner sig i loopen
 delay(500);                           // Lagt in paus för att lugna ner läsandet av ldr
}
